var express = require('express');
var router = express.Router();
let mysql = require('mysql')
let axios = require('axios')
let cheerio = require('cheerio')
let logger  = require('../log/logger')

let db = mysql.createConnection({
  host:'127.0.0.1',
  port:'3306',
  user:'root',
  password:'qlk123456',
  database:'qlk'
})

db.connect((err)=>{
  if(err){
    console.log('数据库链接失败')
  }else{
    console.log('数据库链接成功')
  }
})
/**
 这里爬取网页数据=>方法
*/ 
let jianlaiUrl='http://www.jianlaixiaoshuo.com/'
let getJianlai = async ()=>{
  db.query('truncate table book',(err)=>{
    if(err){
      logger.info(err)
    }
  })
  let res = await axios.get(jianlaiUrl)
  let $ = cheerio.load(res.data)
  $('body > div:nth-child(2) > div > dl > dd > a').each((i,ele)=>{
    let nodename = `http://www.jianlaixiaoshuo.com${$(ele).attr('href')}`
    let nodehref = $(ele).text()
    // console.log(nodename,nodehref)
    let jianlaisql = `insert into book(nodename,nodehref) values (?,?)`
    db.query(jianlaisql,[nodename,nodehref],(err,row)=>{
      if(err){
        // console.log(err)
        logger.info(err)
        // console.log('插入数据失败')
        return false;
      }else{
        // console.log(row)
        logger.info(err)
        console.log('插入数据成功')
        // response.send(true)      
      }
    })
  })
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});
router.get('/reset',function(req, res, next) {
  // console.log('走这里')
  getJianlai()
  res.send('true')
});

module.exports = router;
